using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy4 : MonoBehaviour
{
    // Start is called before the first frame update

    public float lookRadius;    
    private Transform traget;
    NavMeshAgent agent;
    private Animator anim;
    private Vector3 initialPos;

    void Start()
    {

        traget = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        initialPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        float distance = Vector3.Distance(traget.position, transform.position);
        if (distance <= lookRadius)
        {
            agent.SetDestination(traget.position);
            anim.SetTrigger("targetOn");
        }
        else
        {
            agent.SetDestination(initialPos);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            anim.SetTrigger("attack");
        }
    }
   


}